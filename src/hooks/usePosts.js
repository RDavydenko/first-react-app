import { useMemo } from "react";

export const usePosts = (posts, sort, query) => {
    return useMemo(() => {
        let result = [];

        // sorting
        if (sort !== "") {
            result = [...posts].sort((a, b) => a[sort].localeCompare(b[sort]));
        } else {
            result = [...posts];
        }

        // filtering
        result = result.filter(post => post.title.toLowerCase().includes(query.toLowerCase()));

        return result;
    }, [sort, query, posts]);
};