import React from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import PostItem from "./PostItem";

export default function PostList({ posts, title, onRemove }) {
    return (
        <div>
            {posts.length !== 0
                ? <h1 style={{ textAlign: "center" }}>
                    {title}
                 </h1>
                : <h1 style={{textAlign: "center"}}>Посты не найдены!</h1>
            }
            <TransitionGroup>
                {posts.map((post, index) =>
                    <CSSTransition
                        key={post.id}
                        timeout={500}
                        classNames="post"
                    >
                        <PostItem post={post} index={index + 1} onRemove={onRemove} />
                    </CSSTransition>
                )}
            </TransitionGroup>
        </div>
    )
}
