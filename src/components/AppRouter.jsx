import React, { useContext } from 'react'
import { useRoutes } from "react-router-dom";
import { AuthContext } from '../context';
import { privateRoutes, publicRoutes } from '../router/routes';

const AppRouter = () => {
    const { isAuth } = useContext(AuthContext);
    const routes = isAuth
        ? privateRoutes
        : publicRoutes;

    const element = useRoutes(routes);
    return element;
}

export default AppRouter
