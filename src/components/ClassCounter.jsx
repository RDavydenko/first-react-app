import React from 'react'

class ClassCounter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 1
		};

		this.increment = this.increment.bind(this);
		this.decrement = this.decrement.bind(this);
	}

	increment() {
		this.setState({ count: this.state.count + 1 });
		console.log(this.state.count);
	}

	decrement() {
		this.setState({ count: this.state.count - 1 });
		console.log(this.state.count);
	}

	render() {
		return (
			<div>
				<h2>Count: {this.state.count}</h2>
				<button onClick={this.increment}>Increment</button>
				<button onClick={this.decrement}>Decrement</button>
			</div>
		);
	}
}

export default ClassCounter;