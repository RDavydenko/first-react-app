import React, { useState } from 'react'

const Counter = () => {
    const [count, setCount] = useState(1);
    function increment() {
      setCount(count + 1);
      console.log(count);
    }
  
    function decrement() {
      setCount(count - 1);
      console.log(count);
    }

    return (
        <div>
            <h2>Count: {count}</h2>
            <button onClick={increment}>Increment</button>
            <button onClick={decrement}>Decrement</button>
        </div>
    )

};

export default Counter;