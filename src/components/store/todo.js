import { makeAutoObservable } from "mobx";

class Todo {
    todos = [
        { id: 1, title: "Title 1", completed: false },
        { id: 2, title: "Title 2", completed: false },
        { id: 3, title: "Title 3", completed: true },
    ];

    constructor() {
        makeAutoObservable(this, {}, { deep: true });
    }

    addTodo(todo) {
        this.todos.push(todo);
    }

    removeTodo(todo) {
        this.todos = this.todos.filter(x => x.id !== todo.id);
    }

    completeTodo(id) {
        this.todos = this.todos.map(x => x.id === id ? { ...x, completed: !x.completed } : x);
    }
}

export default new Todo();