import React, { useState } from 'react'
import MyButton from './UI/button/MyButton';
import MyInput from './UI/input/MyInput';

const PostForm = ({ onCreate }) => {
    const [post, setPost] = useState({ title: "", body: ""});

    function addNewPost(e) {
      e.preventDefault();
  
      onCreate({...post, id: Date.now()});
      setPost({title: "", body: ""});
    }

    return (
        <div>
            <form>
                <MyInput
                    type="text"
                    placeholder="Название поста"
                    value={post.title}
                    onChange={(e) => setPost({...post, title: e.target.value})}
                />
                <MyInput
                    type="text"
                    placeholder="Описание поста"
                    value={post.body}
                    onChange={(e) => setPost({...post, body: e.target.value})}
                />
                <MyButton onClick={addNewPost}>Создать пост</MyButton>
            </form>
        </div>
    )
}

export default PostForm
