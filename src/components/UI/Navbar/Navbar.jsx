import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { AuthContext } from '../../../context';
import MyButton from '../button/MyButton';
import "./Navbar.css"

function Navbar() {
    const { setIsAuth } = useContext(AuthContext);
    
    const logout = () => {
        localStorage.setItem("auth", false);
        setIsAuth(false);
    };

    return (
        <div className="navbar">
            <MyButton onClick={logout}>Выйти</MyButton>
            <div className="navbar__links">
                <Link to="/about">About</Link>
                <Link to="/posts">Posts</Link>
                <Link to="/todo">Todo</Link>
                <Link to="/counter">Counter</Link>
            </div>
        </div>
    )
}

export default Navbar;
