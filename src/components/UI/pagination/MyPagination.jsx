import React from 'react'
import { getPagesArray } from '../../../utils/pages';
import MyButton from '../button/MyButton'

const MyPagination = ({ pagesCount, pageNumber, onChange }) => {
    const pagesArray = getPagesArray(pagesCount);

    return (
        <div style={{marginTop: "30px", marginBottom: "30px"}}>
            {pagesArray.map(num =>
                <MyButton
                    key={num}
                    style={{marginRight: "5px"}}
                    active={num === pageNumber}
                    onClick={() => onChange(num)}
                >{num}</MyButton>
            )}
        </div>
    )
}

export default MyPagination
