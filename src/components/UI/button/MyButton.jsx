import React from 'react'
import classes from "./MyButton.module.css";

export default function MyButton({ children: text, active, ...props }) {
    const rootClasses = [classes.myBtn];

    if (active) {
        rootClasses.push(classes.active);
    }

    return (
        <button {...props} className={rootClasses.join(" ")}>
            {text}
        </button>
    )
}
