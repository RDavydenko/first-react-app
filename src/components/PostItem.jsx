import React from 'react'
import MyButton from './UI/button/MyButton';
import { useNavigate } from "react-router-dom";

const PostItem = ({ post, index, onRemove }) => {
    const navigate = useNavigate();

    return (
        <div>
            <div className="post">
                <div className="post__content">
                    <strong>{post.id}. {post.title}</strong>
                    <div>{post.body}</div>
                </div>
                <div className="post_btns">
                    <MyButton onClick={() => navigate(`/posts/${post.id}`)}>Открыть</MyButton>
                    <MyButton onClick={() => onRemove(post)}>Удалить</MyButton>
                </div>
            </div>
        </div>
    )
};

export default PostItem;
