import About from "../pages/About";
import Counter from "../pages/Counter";
import Error from "../pages/Error";
import Login from "../pages/Login";
import PostIdPage from "../pages/PostIdPage";
import Posts from "../pages/Posts";
import Todo from "../pages/Todo";

export const privateRoutes = [
    { path: "/about", element: <About /> },
    { path: "/posts", element: <Posts />, exact: true },
    { path: "/posts/:id", element: <PostIdPage />, exact: true },
    { path: "/error", element: <Error /> },
    { path: "/counter", element: <Counter /> },
    { path: "/todo", element: <Todo /> },
    { path: "/", element: <Posts />, exact: true },
    { path: "*", element: <Error />, exact: true },
];

export const publicRoutes = [
    { path: "*", element: <Login /> },
];