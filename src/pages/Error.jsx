import React from 'react'

const Error = () => {
    return (
        <h1>
            Ошибка! Страница не найдена
        </h1>
    )
}

export default Error;
