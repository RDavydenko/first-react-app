import { observer } from 'mobx-react-lite';
import React, { useState } from 'react'
import counter from '../components/store/counter';

const Counter = observer(() => {
    const [count, setCount] = useState(0);

    return (
        <div>
            <h1>
                Счетчик: {counter.count}
            </h1>
            <h2>Total: {counter.total}</h2>
            <button onClick={() => counter.increment()}>Увеличить</button>
            <button onClick={() => counter.decrement()}>Уменьшить</button>
        </div>
    )
});

export default Counter;
