import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import PostService from '../api/PostService';
import Loader from '../components/UI/Loader/Loader';
import { useFetching } from '../hooks/useFetching';

const PostIdPage = () => {
    const params = useParams();
    const id = params.id;

    const [post, setPost] = useState({title: "", body: ""});
    const [comments, setComments] = useState([]);

    const [fetchPostById, isLoading, error] = useFetching(async () => {
        setPost(await PostService.getById(id));
    });

    const [fetchComments, isCommentsLoading, commentsError] = useFetching(async () => {
        setComments(await PostService.getCommentsById(id));
    });
    
    useEffect(() => {
        fetchPostById();
        fetchComments();
    }, []);

    return (
        <div>
            <h1>Вы открыли страницу поста c ID {id}</h1>
            {isLoading && <Loader />}
            <h1>{post.title}</h1>
            <p>{post.body}</p>
            <br />
            <h2>Комментарии</h2>
            {isCommentsLoading && <Loader />}
            {comments.map(c => 
                <div key={c.id} style={{marginBottom: "5px"}}>
                    <h5>{c.email}</h5>
                    <div>{c.name}</div>
                </div>    
            )}
        </div>
    )
}

export default PostIdPage
