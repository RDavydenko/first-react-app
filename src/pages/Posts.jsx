import { useState, useEffect, useRef } from "react";
import PostForm from "../components/PostForm.jsx";
import PostFilter from "../components/PostFilter";
import PostList from "../components/PostList";
import "./Posts.css";
import MyModal from "../components/UI/MyModal/MyModal";
import MyButton from "../components/UI/button/MyButton";
import { usePosts } from "../hooks/usePosts";
import PostService from "../api/PostService";
import Loader from "../components/UI/Loader/Loader";
import { useFetching } from "../hooks/useFetching";
import { getPageCount } from "../utils/pages";
import MyPagination from "../components/UI/pagination/MyPagination";
import { useObserver } from "../hooks/useObserver.js";
import MySelect from "../components/UI/select/MySelect.jsx";

const Posts = () => {
    const [posts, setPosts] = useState([]);
    const [filter, setFilter] = useState({ sort: "", query: "" });
    const [modalVisible, setModalVisible] = useState(false);
    const [totalPages, setTotalPages] = useState(0);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const lastElementRef = useRef();

    const sortedAndFilteredPosts = usePosts(posts, filter.sort, filter.query);

    const createPost = (post) => {
        setPosts([...posts, post]);
        setModalVisible(false);
    };

    const removePost = ({ id }) => {
        setPosts(posts.filter(p => p.id !== id));
    };

    const [fetchPosts, isPostsLoading, error] = useFetching(async () => {
        const [newPosts, count] = await PostService.getAll(limit, page);
        setTotalPages(getPageCount(count, limit));
        setPosts([...posts, ...newPosts]);
    });

    // const changePage = (num) => {
    //     setPage(num);
    // }


    useObserver(lastElementRef, 
        page < totalPages, 
        isPostsLoading, 
        () => setPage(page + 1)
    );
    
    useEffect(() => {
        fetchPosts();
    }, [page, limit]);


    return (
        <div className="Posts">
            <MyButton style={{ marginTop: "10px" }} onClick={() => setModalVisible(true)}>Добавить пост</MyButton>
            <MyModal
                visible={modalVisible}
                setVisible={setModalVisible}
            >
                <PostForm onCreate={createPost} />
            </MyModal>
            <PostFilter filter={filter} setFilter={setFilter} />
            <MySelect 
                value={limit}
                onChange={(val) => setLimit(val)}
                defaultValue="Кол-во элементов"
                options={[
                    {value: 5, name: "5"},
                    {value: 10, name: "10"},
                    {value: 25, name: "25"},
                    {value: -1, name: "Показать все"},
                ]}
            />
            {error && <h1>{error}</h1>}
            <PostList posts={sortedAndFilteredPosts} title="Список постов про JS" onRemove={removePost} />
            <div ref={lastElementRef} style={{height: "20px" }} /> 
            {isPostsLoading && <div style={{display: "flex", justifyContent: "center", marginTop: "50px"}}><Loader /></div>}
            {/* <MyPagination
                pageNumber={page}
                pagesCount={totalPages}
                onChange={changePage}
            /> */}
        </div>
    );
}

export default Posts;